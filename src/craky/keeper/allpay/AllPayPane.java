package craky.keeper.allpay;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.hibernate.criterion.Criterion;

import craky.chart.Bar3DChart;
import craky.chart.Chart3D;
import craky.chart.Pie3DChart;
import craky.componentc.JCCheckBox;
import craky.componentc.JCCheckBoxList;
import craky.keeper.KeeperApp;
import craky.keeper.KeeperConst;
import craky.keeper.KeeperMgr;
import craky.keeper.KeeperPane;

public class AllPayPane extends KeeperPane
{
    private static final long serialVersionUID = -381623737074535966L;
    
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM");
    
    public static final String TYPE = "\u5168\u90E8";
    
    private Map<String, Number> pieMap, barMap;
    
    private Chart3D pieChart, barChart;
    
    public AllPayPane(KeeperApp keeper, KeeperMgr mgr)
    {
        super(keeper, mgr);
        setImageOnly(true);
    }
    
    protected JComponent createRight()
    {
        right = new JPanel();
        pieChart = new Pie3DChart(pieMap = new HashMap<String, Number>(), 10, "\u6D88\u8D39\u7C7B\u522B\u7EDF\u8BA1");
        barChart = new Bar3DChart(barMap = new HashMap<String, Number>(), 10, 10, "\u6D88\u8D39\u65E5\u671F\u7EDF\u8BA1");
        right.setPreferredSize(new Dimension(400, -1));
        right.setBorder(new CompoundBorder(new LineBorder(new Color(84, 165, 213)), new EmptyBorder(5, 3, 5, 3)));
        right.setLayout(null);
        right.setBackground(Color.WHITE);
        right.setOpaque(true);
        ((Pie3DChart)pieChart).setDataFormat(KeeperConst.AMOUNT_FORMAT);
        pieChart.setMaximumSize(new Dimension(1000, 300));
        ((Bar3DChart)barChart).setFormat(KeeperConst.AMOUNT_FORMAT);
        right.add(pieChart);
        right.add(barChart);
        right.setVisible(false);
        right.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                layoutStat();
            }
        });
        
        return right;
    }
    
    private void layoutStat()
    {
        final int gap = 20;
        Insets insets = right.getInsets();
        int width = right.getWidth() - insets.left - insets.right;
        int height = right.getHeight() - insets.top - insets.bottom;
        int maxPieHeight = pieChart.getMaximumSize().height;
        int pieHeight = 0, barHeight = 0;
        
        if(!barChart.isVisible())
        {
            pieHeight = Math.min(height, maxPieHeight);
        }
        else
        {
            pieHeight = Math.min(maxPieHeight, (height - gap) / 2);
            barHeight = height - pieHeight - gap;
        }
        
        pieChart.setBounds(insets.left, insets.top, width, pieHeight);
        barChart.setBounds(insets.left, insets.top + pieHeight + gap, width, barHeight);
    }
    
    protected Criterion[] createCriterions(String type)
    {
        return super.createCriterions(null);
    }
    
    protected void resetRowValues(AllPay pay)
    {
        super.resetRowValues(pay);
        pay.setType(String.valueOf(cpType.getSelectedItem()));
    }
    
    protected List<AllPay> filterResult(List<AllPay> list)
    {
        JCCheckBoxList checkList = cpFType.getCheckedList();
        
        if(!checkList.isSelectedAll() && !checkList.isSelectedEmpty())
        {
            List<?> selTypes = cpFType.getSelectedItems();
            
            for(int i = list.size() - 1; i >= 0; i--)
            {
                if(!selTypes.contains(list.get(i).getType()))
                {
                    list.remove(i);
                }
            }
        }
        
        return list;
    }
    
    protected JCCheckBox createRightSplitButton()
    {
        btnShowRight = new JCCheckBox();
        btnShowRight.setPreferredSize(new Dimension(6, -1));
        btnShowRight.setIcon(emptyIcon);
        btnShowRight.setSelectedIcon(emptyIcon);
        btnShowRight.setRolloverIcon(splitRight);
        btnShowRight.setRolloverSelectedIcon(splitLeft);
        btnShowRight.setPressedIcon(splitRight);
        btnShowRight.setPressedSelectedIcon(splitLeft);
        btnShowRight.setFocusable(false);
        btnShowRight.setToolTipText(SHOW_STAT);
        btnShowRight.addItemListener(splitListener);
        return btnShowRight;
    }
    
    public void updateCategory()
    {
        resetTypes();
    }
    
    public void refreshStat()
    {
        if(!isShowStat())
        {
            return;
        }
        
        pieMap.clear();
        barMap.clear();
        String type;
        String date;
        float amount;
        
        for(AllPay pay: data)
        {
            amount = pay.getAmount();
            type = pay.getType();
            date = DATE_FORMAT.format(pay.getDate());
            
            if(pieMap.containsKey(type))
            {
                pieMap.put(type, pieMap.get(type).doubleValue() + amount);
            }
            else
            {
                pieMap.put(type, amount);
            }
            
            if(barMap.containsKey(date))
            {
                barMap.put(date, barMap.get(date).doubleValue() + amount);
            }
            else
            {
                barMap.put(date, amount);
            }
        }
        
        pieChart.setDatasAndRepaint(pieMap);
        barChart.setDatasAndRepaint(barMap);
    }

    public String getType()
    {
        return TYPE;
    }
}