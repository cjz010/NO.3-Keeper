package craky.keeper.allpay;

import java.util.List;

import org.hibernate.criterion.Criterion;

import craky.keeper.category.Category;
import craky.keeper.category.CategoryDAO;
import craky.keeper.util.HibernateUtil;

public class AllPayDAO
{
    @SuppressWarnings("unchecked")
    public static List<AllPay> getAllPays(Criterion...criterions)
    {
        return (List<AllPay>)HibernateUtil.getDatas(AllPay.class, criterions);
    }
    
    public static void addAllPay(AllPay allPay, Category category)
    {
        HibernateUtil.insert(allPay);
        CategoryDAO.afterInsertItem(category);
    }
    
    public static void deleteAllPay(AllPay allPay, Category category)
    {
        HibernateUtil.delete(allPay);
        CategoryDAO.afterRemoveItem(category);
    }
    
    public static void updateAllPay(AllPay allPay)
    {
        HibernateUtil.update(allPay);
    }
}