package craky.keeper.category;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import craky.keeper.util.HibernateUtil;

public class CategoryDAO
{
    @SuppressWarnings("unchecked")
    public static List<Category> getCategorys(Criterion...criterions)
    {
        List<Category> categorys = (List<Category>)HibernateUtil.getDatas(Category.class, criterions);
        Collections.sort(categorys);
        return categorys;
    }
    
    public static List<Category> getSystemCategorys()
    {
        return getCategorys(Restrictions.eq("type", Category.TYPE_SYSTEM));
    }
    
    public static List<Category> getCustomCategorys()
    {
        return getCategorys(Restrictions.eq("type", Category.TYPE_CUSTOM));
    }
    
    public static void addCategory(Category category)
    {
        HibernateUtil.insert(category);
    }
    
    public static void deleteCategory(Category category)
    {
        HibernateUtil.delete(category);
    }
    
    public static void updateCategory(Category category)
    {
        HibernateUtil.update(category);
    }
    
    public static void afterInsertItem(Category category)
    {
        category.setCount(category.getCount() + 1);
        CategoryDAO.updateCategory(category);
    }
    
    public static void afterRemoveItem(Category category)
    {
        category.setCount(category.getCount() - 1);
        CategoryDAO.updateCategory(category);
    }
}