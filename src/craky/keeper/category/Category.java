package craky.keeper.category;

import java.io.Serializable;

public class Category implements Serializable, Comparable<Category>
{
    private static final long serialVersionUID = 2085530002657221873L;
    
    public static final int TYPE_SYSTEM = 0;
    
    public static final int TYPE_CUSTOM = 1;
    
    public static final String SYSTEM_DES = "\u7CFB\u7EDF\u7C7B\u522B";
    
    public static final String CUSTOM_DES = "\u7528\u6237\u7C7B\u522B";

    private int id;
    
    private String name;
    
    private int type;
    
    private long count;
    
    public Category() {}
    
    public Category(String name)
    {
        this.name = name;
        this.type = TYPE_CUSTOM;
        this.count = 0;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public long getCount()
    {
        return count;
    }

    public void setCount(long count)
    {
        this.count = count;
    }
    
    public String getTypeName()
    {
        String tn = null;
        
        switch(type)
        {
            case TYPE_SYSTEM:
            {
                tn = SYSTEM_DES;
                break;
            }
            case TYPE_CUSTOM:
            {
                tn = CUSTOM_DES;
                break;
            }
        }
        
        return tn == null? "": tn;
    }
    
    public void setTypeName(String typeName)
    {
        setType(getTypeByName(typeName));
    }
    
    public static int getTypeByName(String typeName)
    {
        if(typeName.equals(SYSTEM_DES))
        {
            return TYPE_SYSTEM;
        }
        else
        {
            return TYPE_CUSTOM;
        }
    }
    
    public String toString()
    {
        return name;
    }
    
    public int compareTo(Category category)
    {
        return type == category.getType()? id - category.getId(): category.getType() - type;
    }
}