package craky.keeper.extra;

import javax.swing.JComponent;

import craky.keeper.KeeperApp;
import craky.keeper.KeeperMgr;
import craky.keeper.KeeperPane;
import craky.keeper.allpay.AllPay;

public class ExtraPane extends KeeperPane
{
    private static final long serialVersionUID = 408500291307459044L;
    
    public static final String TYPE = "\u989D\u5916";
    
    public ExtraPane(KeeperApp keeper, KeeperMgr mgr)
    {
        super(keeper, mgr);
        setImageOnly(true);
    }
    
    protected JComponent createCenter()
    {
        super.createCenter();
        table.getColumnModel().removeColumn(table.getColumnModel().getColumn(3));
        return center;
    }
    
    protected JComponent createBottom()
    {
        super.createBottom();
        ecType.setVisible(false);
        return bottom;
    }
    
    protected JComponent createTop()
    {
        super.createTop();
        ecFType.setVisible(false);
        return top;
    }
    
    protected void resetRowValues(AllPay pay)
    {
        super.resetRowValues(pay);
        pay.setType(TYPE);
    }
    
    public String getType()
    {
        return TYPE;
    }
}
